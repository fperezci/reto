FROM node:latest

# Bundle app source
COPY reto reto/

# Create app directory
WORKDIR /reto
RUN npm install

# If you are building your code for production
# RUN npm ci --only=production
EXPOSE 3500

CMD [ "npm", "run", "start" ]