const request = require('request');

const callexternalapi = async (url, callback) => {
    request(url, { json: true }, (err, res, body) => {
        if(err) {
            return callback(err);
        }
        else {
            return callback(body);
        }
    })
}

module.exports.callApi =  callexternalapi;