const joi = require('joi');

module.exports = {
    reemplazarSchema: joi.object({
        movie: joi.string().required(),
        find: joi.string().required(),
        replace: joi.string().required(),
    }),
    buscarSchema: joi.object({
        patron_busqueda: joi.string().required()
    })
}