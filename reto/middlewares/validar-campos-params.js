module.exports = (schema) => {
    return async (ctx, next) => {
        try {
            await schema.validateAsync(ctx.params);
            next();
            
        } catch(error) {

            ctx.status = 400;
            ctx.body = {
                ok: false,
                error: error.message
            };
        }
    }

}