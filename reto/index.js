'use strict';
const logger = require('logger').createLogger('dev.log');
logger.info('Iniciando servidor');

logger.info('Cargando variables de entorno');
require('dotenv').config();

const { dbConnection } = require('./database/config');

logger.info('Estableciendo conexion con mongoDB');

// Base de datos
dbConnection();

logger.info('Cargando koa');

const koa = require('koa');
const Router = require('@koa/router');
const koaBody = require('koa-body');

const app = new koa();
const route = new Router();

app.use(koaBody());
// app.use(ctx => {
//     ctx.body = `Request Body: ${JSON.stringify(ctx.request.body)}`;
//   });

logger.info('Cargando rutas');
// Rutas
route.use('/pelicula', require('./routes/peliculas'));
app.use(route.routes());

logger.info('Levantando servidor en el puerto ', process.env.PORT);
app.listen( process.env.PORT );