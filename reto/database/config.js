const mongoose = require('mongoose');
const logger = require('logger').createLogger('dev.log');

const dbConnection = async() => {

    try {

        await mongoose.connect( process.env.DB_CNN, {} );
        logger.info('Enlace correcto a MongoDB');

    } catch (error) {
        logger.error('Ha ocurrido un error al conectarse a MongoBD');
        logger.error(error);
        throw new Error('Error a la hora de iniciar la BD ver logs');
    }

}

module.exports = {
    dbConnection
}