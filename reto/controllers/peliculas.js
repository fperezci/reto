const Pelicula = require('../models/peliculas');
const logger = require('logger').createLogger('dev.log');
const fetch = require('node-fetch');

/**
 * Buscador de películas:

    Método GET

    El valor a buscar debe ir en la URL de la API

    Adicionalmente puede ir un header opcional que contenga el año de la película.

    Almacenar en una BD Mongo, la siguiente info:
    Title, Year, Released, Genre, Director, Actors, Plot, Ratings

    El registro de la película solo debe estar una vez en la BD.

    Devolver la información almacenada en la BD.
 * @param {*} ctx 
 */
const getBuscar = async ctx => {

    logger.info('Iniciando vista buscar peliculas');
    
    const year = Number(ctx.header.year) || 0;
    
    let resultData;
    
    const { patron_busqueda } = ctx.params;

    if ( patron_busqueda == undefined ) {
        ctx.status = 400;
        ctx.body = {
            ok: true,
            msj: 'El parametro de bùsqueda es requerido',
        };
    }
    else {
        url_api = process.env.URL_API + patron_busqueda

        if (year != 0) {
            url_api += '&year=' + year;
        }
        
        const url = []
        url[0] = url_api;

        const response = url.map( async url => {
            try {
                const response = await fetch(url);
                const json = await response.json();
                resultData = json.results;
                
                for (let i = 0; i < resultData.length; i++) {
                    const datos = {
                        title: resultData[i].original_title,
                        year: Number(resultData[i].release_date.substring(0,4)),
                        released: resultData[i].release_date,
                        genre: '', 
                        director: '',
                        actors: '',
                        plot: resultData[i].overview,
                        ratings   : ''
                    }

                    const { title } = datos;
            
                    logger.info('Verificar si la pelicula encontrada ya existe en la base de datos de MongoDB');
                
                    const encontrarPelicula = await Pelicula.findOne({ title });
                
                    if ( ! encontrarPelicula ) {
                        
                        logger.info('Se ingresa la pelicula en MongoDB');
                
                        const pelicula = new Pelicula( datos );
                
                        await pelicula.save();
                
                    }
                }
   
            } catch (error) {
                logger.error('Error al guardar en mongoDB');             
                logger.error(error);             
            }

        });

        return Promise.all(response).then(async () => {
            const [ peliculas, total ] = await Promise.all([
                await Pelicula
                    .find(),
                    // .skip( pagina * 5 - 5 )
                    // .limit( 5 ),
                await Pelicula.count()
            ]);
    
            ctx.status = 200;
            ctx.body = {
                ok: true,
                msj: 'Agregar pelicula',
                peliculas,
                total
            };
            logger.info('Carga correcta de vista buscar peliculas');
            //return ctx
        });
    }
}


/**
 * Obtener todas las películas:

    Método GET

    Se deben devolver todas las películas que se han guardado en la BD.
    Si hay más de 5 películas guardadas en BD, se deben paginar los resultados de 5 en 5
    El número de página debe ir por header.
 */
const getListar = async ctx => {

    logger.info('Iniciando vista listar peliculas');

    const pagina = Number(ctx.header.pagina) || 0;

    if (pagina != 0) {
        logger.info('Buscando listado y total de peliculas desde MongoDB')

        // Promesa optimizada que retorna 2 querys
        const [ peliculas, total ] = await Promise.all([
            await Pelicula
                .find( {}, 'title year' )
                .skip( pagina * 5 - 5 )
                .limit( 5 ),
            await Pelicula.count()
        ]);
        ctx.status = 200;
        ctx.body = {
            msj: 'listado de peliculas',
            peliculas,
            total,
        }
    
        logger.info('Carga correcta de vista listar peliculas');
    }

    else {
        ctx.status = 200;
        ctx.body = {
            msj: 'No se detectó el valor correcto para el parámetro página',
        }
    
        logger.error('No se detectó el valor correcto para el parámetro página');
    }

    
}

/**
 * Buscar y reemplazar:

    Método POST que reciba en el BODY un object como: P.E: {movie: star wars, find: jedi, replace: CLM Dev }
    Buscar dentro de la BD y obtener el campo PLOT del registro
    Al string del plot obtenido buscar la palabra enviada en el Body (find) y reemplazar todas sus ocurrencias por el campo enviado en el body (replace)
    Devolver el string con las modificaciones del punto anterior 
 */
const getReemplazar = async ctx => {

    logger.info('Iniciando vista reemplazar peliculas');

    const { movie, find, replace } = ctx.request.body;

    const encontrarPelicula = await Pelicula.findOne({ title: movie }, '' );

    if ( !encontrarPelicula ) {
        ctx.status = 400;
        ctx.body = {
            ok: true,
            msj: 'No existe la pelicula'
        };
        logger.info('No existe peliculas');

    } 
    else {

        encontrarPelicula.plot = encontrarPelicula.plot.replace(find, replace);

        const peliculaActualizada = await Pelicula.findByIdAndUpdate( encontrarPelicula._id, encontrarPelicula, { new: true } )

        ctx.status = 200;
        ctx.body = {
            ok: true,
            msj: 'reemplazar',
            peliculaActualizada
        };

        logger.info('Carga correcta de vista reemplazar peliculas');
    }



}

module.exports = {
    getListar,
    getBuscar,
    getReemplazar,
}