const Router = require('@koa/router');
const route = new Router();
const { reemplazarSchema, buscarSchema } = require('../schema/schema');
const validarDatosBody = require('../middlewares/validar-campos-body');
const validarDatosParams = require('../middlewares/validar-campos-params');

const { getListar, getReemplazar, getBuscar } = require('../controllers/peliculas');

// Algo desconocido me impide usar el middlewaye para validar el parametro
// route.get('/buscar/:patron_busqueda?', validarDatosParams(buscarSchema), getBuscar);
route.get('/buscar/:patron_busqueda?', getBuscar);

route.get('/listar', getListar);

route.post('/reemplazar', validarDatosBody(reemplazarSchema), getReemplazar);

module.exports = route.routes();