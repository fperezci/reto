const { Schema, model } = require('mongoose');

// Estructura del Modelo de pelicula de Mongodb
const PeliculaSchema= Schema ({
    title: { type: String, required: true, unique: true },
    year: { type: Number },
    released: { type: Date },
    genre: { type: String },
    director: { type: String },
    actors: { type: String },
    plot: { type: String },
    ratings: { type: String }
});

// Se omite el campo _id
PeliculaSchema.method('toJSON', function() {
    const { _id, __v, ...object } = this.toObject();

    object.uid = _id;
    
    return object;
})

module.exports = model('Pelicula', PeliculaSchema);