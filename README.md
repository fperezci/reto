# Endpoint para Reto
Endpoint para el reto

## Implementación
Para levantar el sitio se implemento Docker

## Creación de Imagen en Docker
docker build -t endpointreto:0.10 .

## Crear contenedor para uso en Docker
docker run --name endpointreto-container -p 3500:3500 -d endpointreto:0.10
